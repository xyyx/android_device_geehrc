#!/bin/bash

cd `dirname $0`
DSTDIR=$1

if [ -z "$DSTDIR" ]
then
    echo "Usage: $0 <sources dir>"
    exit 1
fi

red=$(tput setaf 1) # Red
grn=$(tput setaf 2) # Green
txtrst=$(tput sgr0) # Reset

echo "${grn}Applying build patch${txtrst}"
cat 1.build.patch | patch -d $DSTDIR/build/ -p1 -N -r -

echo "${grn}Applying vendor patch${txtrst}"
cat 2.vendor.patch | patch -d $DSTDIR/vendor/eos/ -p1 -N -r -

echo "${grn}Applying webview patch${txtrst}"
cat 3.webview.patch | patch -d $DSTDIR/frameworks/webview/ -p1 -N -r -

