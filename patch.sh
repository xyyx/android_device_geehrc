#!/bin/bash

cd `dirname $0`
DSTDIR=$1

if [ -z "$DSTDIR" ]
then
    echo "Usage: $0 <sources dir>"
    exit 1
fi

red=$(tput setaf 1) # Red
grn=$(tput setaf 2) # Green
txtrst=$(tput sgr0) # Reset

echo "${red}Linaro patches${txtrst}"
sh patches/linaro/patch.sh $DSTDIR

echo "${red}Prebuilt Chromium patches${txtrst}"
sh patches/prebuilt_chromium/patch.sh $DSTDIR

echo "${grn}Applying build patch${txtrst}"
cat patches/1.build.patch | patch -d $DSTDIR/build/ -p1 -N -r -

#echo "${grn}Applying vendor patch${txtrst}"
#cat patches/2.vendor.patch | patch -d $DSTDIR/vendor/eos/ -p1 -N -r -

#cd $DSTDIR

#find . -name '*.orig' -delete

